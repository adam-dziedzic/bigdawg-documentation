Welcome to BigDAWG documentation
================================

Overview
---------

(Show some conceptual diagram of the BigDAWG system)

.. image:: fig1.png
   :width: 300px
   :alt: BigDAWG notional architecture
   :align: center

(Explain what it does)

.. tip::
	Note for documentation authors:

   This documentation is written in reStructuredText (reST), a markup language similar to markdown, and hosted by ReadTheDocs. The reST text files are committed to git, and then pulled by ReadTheDocs to render the html that you see.

	Here is a reStructuredText markup guide:
	http://www.sphinx-doc.org/en/1.5.1/rest.html

	Here's a general guide to using reST for ReadTheDocs:
	http://restructuredtext.readthedocs.io/en/latest/sphinx_tutorial.html


A simple example
-----------------
(A simple query and result)
	``Code example goes here``

Get the code
-------------

The `source <https://bitbucket.org/aelmore/bigdawgmiddle>`_ is available on BitBucket.

The mailing list for the project is located at google groups: http://groups.google.com/group/bigdawg

(Link to the website)


Contributing
------------

(Contact info, pull requests, etc.)


Contents
--------

.. toctree::
   :maxdepth: 3
   :numbered:

   intro
   getting-started
   internals
   query-language
   admin-ui
   publications