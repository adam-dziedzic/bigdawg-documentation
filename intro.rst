=========================
Introduction and Overview
=========================

Team
----

(Background on ISTC and team)

Polystores
----------

(Background on the problem we are trying to solve)

BigDAWG Approach
----------------

(A few paragraphs on our approach involving islands, engines, casts, etc)

Major BigDAWG Components
------------------------

(diagram of the relationship between islands, engines, etc)
