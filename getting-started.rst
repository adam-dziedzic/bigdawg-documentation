Getting Started with BigDAWG
============================

(A how-to page for using BigDAWG for the first time)

Setup a BigDAWG Cluster with Docker
-----------------------------------

(Steps to use docker)

Run Example Queries
-------------------

(Table of queries. Example usage and output)

Examine the Catalog
-------------------

(Explanation of the important catalog parts)

Use the Administrative Web Interface
------------------------------------

(How to start the web interface and do basic functions)